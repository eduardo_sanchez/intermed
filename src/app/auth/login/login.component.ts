import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isRegister = false;
  isDoctor = false;

  constructor( public dialogRef: MatDialogRef<LoginComponent>, private router: Router ) { }

  ngOnInit() {
  }

  goRegsiter() {
    if( !this.isDoctor ){
      this.router.navigate(['/registro-usuario']);
    }else{
      this.router.navigate(['/registro-usuario']);
    }

    this.dialogRef.close();
  }

}
