import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSecretaryComponent } from './register-secretary.component';

describe('RegisterSecretaryComponent', () => {
  let component: RegisterSecretaryComponent;
  let fixture: ComponentFixture<RegisterSecretaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterSecretaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterSecretaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
