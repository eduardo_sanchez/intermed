import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {

  stepper = 1;

  constructor() { }

  ngOnInit(): void {
  }

  next(){
    this.stepper++;
  }

}
