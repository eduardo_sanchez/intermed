import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { LandingComponent } from './landing/landing.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterDoctorComponent } from './auth/register/register-doctor/register-doctor.component';
import { RegisterUserComponent } from './auth/register/register-user/register-user.component';
import { RegisterSecretaryComponent } from './auth/register/register-secretary/register-secretary.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    RegisterComponent,
    LoginComponent,
    RegisterDoctorComponent,
    RegisterUserComponent,
    RegisterSecretaryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  exports: [
    LoginComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
