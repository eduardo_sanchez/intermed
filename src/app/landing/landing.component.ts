import { Component, OnInit } from '@angular/core';
import { LoginComponent } from '../auth/login/login.component';
import {MatDialog} from '@angular/material/dialog';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '457px',
      height: '631px'
    });
  }

}
